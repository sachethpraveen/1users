const users = {
  John: {
    age: 24,
    desgination: "Senior Golang Developer",
    interests: ["Chess, Reading Comics, Playing Video Games"],
    qualification: "Masters",
    nationality: "Greenland",
  },
  Ron: {
    age: 19,
    desgination: "Intern - Golang",
    interests: ["Video Games"],
    qualification: "Bachelor",
    nationality: "UK",
  },
  Wanda: {
    age: 24,
    desgination: "Intern - Javascript",
    interests: ["Piano"],
    qualification: "Bachaelor",
    nationality: "Germany",
  },
  Rob: {
    age: 34,
    desgination: "Senior Javascript Developer",
    interest: ["Walking his dog, Cooking"],
    qualification: "Masters",
    nationality: "USA",
  },
  Pike: {
    age: 23,
    desgination: "Python Developer",
    interests: ["Listing Songs, Watching Movies"],
    qualification: "Bachaelor's Degree",
    nationality: "Germany",
  },
};

//Q1 Find all users who are interested in playing video games.

function findHobbies(hobby, users) {
  resultArray = Object.entries(users);
  return resultArray.filter((user) => {
    if (user[1].hasOwnProperty("interests")) {
      return (
        user[1].interests.includes("Video Games") ||
        user[1].interests.includes("Playng Video Games")
      );
    }
  });
}

// console.log(findHobbies("Video Games", users));

// Q2 Find all users staying in Germany.

function findCountry(country, users) {
  resultArray = Object.entries(users);
  return resultArray.filter((user) => {
    return user[1].nationality === country;
  });
}

// console.log(findCountry("Germany",users));

//Q3 Sort users based on their seniority level

//for Designation - Senior Developer > Developer > Intern

function sortUsersDesignation(users) {
  const usersArray = Object.entries(users);
  return usersArray.sort((currentUser, nextUser) => {
    if (
      currentUser[1].desgination.split(" ").includes("Intern") &&
      (nextUser[1].desgination.split(" ").includes("Developer") ||
        (nextUser[1].desgination.split(" ").includes("Senior") &&
          nextUser[1].desgination.split(" ").includes("Developer")))
    ) {
      return 1;
    } else if (
      currentUser[1].desgination.split(" ").includes("Developer") &&
      nextUser[1].desgination.split(" ").includes("Senior") &&
      nextUser[1].desgination.split(" ").includes("Developer")
    ) {
      return 1;
    } else {
      return -1;
    }
  });
}

// console.log(sortUsersDesignation(users));

//for Age - 20 > 10
function sortUsersAge(users) {
  const usersArray = Object.entries(users);
  return usersArray.sort((currentUser, nextUser) => {
    return nextUser[1].age - currentUser[1].age;
  });
}

// console.log(sortUsersAge(users));

// Q4 Find all users with masters Degree.
function findQualification(degree, users) {
  const usersArray = Object.entries(users);
  return usersArray.filter((user) => {
    return user[1].qualification.split(" ").includes("Masters");
  });
}

console.log(findQualification("Masters", users));
